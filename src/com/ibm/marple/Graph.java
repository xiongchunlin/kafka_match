package com.ibm.marple;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import com.ibm.marple.Edge.Edge_Type;



/**
 * @author rainkin
 * utils to build edges and to filter nodes and edges
 */
public class Graph {
	
	/**
	 * build argument sharing edges 
	 * @param former syscall invoked earlier
	 * @param latter syscall invoked later 
	 * @return a list of edges between two syscalls
	 */
	public static List<Edge> build_argument_sharing(SyscallRecord former, SyscallRecord latter)
	{
		List<Edge> edges = new ArrayList<Edge>(); // store all edges built
		
		// find the same parameters between two syscalls
		Set<String> from_parameters = new HashSet<String>(former.parameters);
		Set<String> to_parameters = new HashSet<String>(latter.parameters);
		Set<String> intersection = new HashSet<String>(from_parameters);
		intersection.retainAll(to_parameters);
//		System.out.println("==========" + from_parameters + "----" + to_parameters + "----" + intersection);
		
		// build argument_sharing Edges
		for (String arg : intersection)
		{
			// check whether the arg is meaningful
			if (!_is_arg_meaningless(arg)){
				
				Edge new_edge = new Edge(former, latter, Edge.Edge_Type.ARGUMENT_SHARING, arg);
				
				//check whether this new edge should be ignored
				if (!_is_edge_meaningless(new_edge, edges)) {
					edges.add(new_edge);
				}
				
			}
				
		}
		
		return edges;
	}
	
	/**
	 * build data dependence edges 
	 * @param former syscall invoked earlier
	 * @param latter syscall invoked later 
	 * @return a list of edges between two syscalls
	 */
	public static List<Edge> build_data_dependence(SyscallRecord former, SyscallRecord latter)
	{
		Set<Edge> distinct_edges = new HashSet<Edge>(); // store distinct edges between two syscalls 
		List<Edge> edges = new ArrayList<Edge>(); // store all edges built
		
		// build data_dependence Edges
		String from_retval = former.retval;
		for (String to_arg : new HashSet<String>(latter.parameters)) {
			
			if (from_retval.equals(to_arg)) {
				String arg = from_retval;
				
				// check whether the arg is meaningful
				if (!_is_arg_meaningless(arg)){
					
					Edge new_edge = new Edge(latter, former, Edge.Edge_Type.DATA_DEPENDENCE, arg);
					
					//check whether this new edge should be ignored
					if (!_is_edge_meaningless(new_edge, edges)) {
						edges.add(new_edge);
					}
					
				}
			}
		}
		
		return edges;
	}
	
	
	/**
	 * whether the value of argument is meaningful
	 * @param value the value of argument
	 * @return if meaningless, return true, otherwise return false
	 */
	private static Boolean _is_arg_meaningless(String arg)
	{
		Set<String> meaningless_args = new HashSet<String>();
		meaningless_args.add("FFFFFFFF");
		meaningless_args.add("00000000");
		meaningless_args.add("0000");
		
		return meaningless_args.contains(arg);
	}
	
	
	/**
	 * whether the edge should be removed
	 * @param args
	 * @return if meaningless, return true, otherwise return false
	 */
	private static Boolean _is_edge_meaningless(Edge edge, List<Edge> edges)
	{
		// check whether edge is duplicated bwtween two syscalls
		String from_syscall_name = edge.fromRecord.name;
		String to_syscall_name = edge.toRecord.name;
		
		Boolean is_duplicated = edges.contains(edge); // remove duplicate edges between two syscalls
		Boolean is_same_syscall_name = from_syscall_name.equals(to_syscall_name); // remove edges build between two syscalls whose name are the same 

		return is_duplicated || is_same_syscall_name;
	}

}

