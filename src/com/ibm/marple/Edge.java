package com.ibm.marple;



/**
 * Class that store the relationship between two syscalls
 */
public class Edge {
	
	public enum Edge_Type {ARGUMENT_SHARING, DATA_DEPENDENCE};
	
	public SyscallRecord fromRecord;
	
	public SyscallRecord toRecord;
	
	public Edge_Type type;
	
	public String parameter;

	public Edge(SyscallRecord fromRecord, SyscallRecord toRecord,
			Edge_Type type, String parameter) {
		super();
		this.fromRecord = fromRecord;
		this.toRecord = toRecord;
		this.type = type;
		this.parameter = parameter;
	}

	@Override
	public boolean equals(Object obj) {  
		if (!(obj instanceof Edge))  
            return false;
		
        if (obj == this)  
            return true;  
        
        Edge edge = (Edge)obj;
   
        if (this.fromRecord == edge.fromRecord && this.toRecord == edge.toRecord && this.type == edge.type && this.parameter == edge.parameter) {
        	return true;
		}
		
        return false;
          
    }  

}
