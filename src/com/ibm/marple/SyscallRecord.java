package com.ibm.marple;

import java.util.ArrayList;
import java.util.Set;


import com.bbn.tc.schema.avro.UUID;

public class SyscallRecord {

	public ArrayList<String> id;
	public String name;
	public ArrayList<Long> sequence;
	public ArrayList<Long> TimestampMicros;
	public Set<String> parameters;
	public Integer pid;
	public String processName;
	public String retval;

	
	public SyscallRecord(String id2,String name,Set<String> parameters,Long TimestampMicros,Long sequence)
	{
		this.id = new ArrayList<String>();
		this.TimestampMicros = new ArrayList<Long>();
		this.sequence = new ArrayList<Long>();
		this.name = name;
//		System.out.println(TimestampMicros);
		
		this.id.add(id2);
		this.parameters = parameters;
		this.TimestampMicros.add(TimestampMicros);
		this.sequence.add(sequence);
	}
	
	public void add(SyscallRecord b){
		this.id.addAll(b.id);
		this.parameters.addAll(b.parameters);
		this.TimestampMicros.addAll(TimestampMicros);
		this.sequence.addAll(sequence);
	}
	
}
