package com.ibm.marple;

import java.util.HashMap;

public class Utils {
	
	public static HashMap<String, String> syscall_to_type = new HashMap<String, String>(); // map from syscall name to name of object processed by the syscall
	
	static
	{	
		// Device Type
		String [] device_type_syscalls = {"NtDeviceIoControlFile"};
		for (String syscall_name : device_type_syscalls){
			syscall_to_type.put(syscall_name, "Device");
		}
		
		// Graphics Type
		String [] graphics_type_syscalls = {"NtGdiGetDeviceCaps", "NtGdiCreateDIBSection", "GreSelectBitmap", "NtGdiExtGetObjectW", "NtGdiDeleteObjectApp"};
		for (String syscall_name : graphics_type_syscalls){
			syscall_to_type.put(syscall_name, "Graphics");
		}
		
		// Synchronization Type
		String [] synchronization_type_syscalls = {"NtWaitForSingleObject"};
		for (String syscall_name : synchronization_type_syscalls){
			syscall_to_type.put(syscall_name, "Synchronization");
		}
		
		// FileSystem Type (file or directory)
		String [] filesystem_type_syscalls = {"NtQueryVolumeInformationFile", "NtOpenFile", "NtCreateNamedPipeFile"};
		for (String syscall_name : filesystem_type_syscalls){
			syscall_to_type.put(syscall_name, "FileSystem");
		}
		
		// Object Type
		String [] object_type_syscalls = {"NtClose", "NtDuplicateObject"};
		for (String syscall_name : object_type_syscalls){
			syscall_to_type.put(syscall_name, "Object");
		}
		
		// Process Type (process or thread)
		String [] process_type_syscalls = {"NtCreateUserProcess", "NtQueryInformationProcess", "NtResumeThread"};
		for (String syscall_name : process_type_syscalls){
			syscall_to_type.put(syscall_name, "Process");
		}
		
		// Registry Type
		String [] registry_type_syscalls = {"NtQueryValueKey"};
		for (String syscall_name : registry_type_syscalls){
			syscall_to_type.put(syscall_name, "Registry");
		}
		
		// Memory Type
		String [] memory_type_syscalls = {"NtAllocateVirtualMemory", "NtWriteVirtualMemory"};
		for (String syscall_name : memory_type_syscalls){
			syscall_to_type.put(syscall_name, "Memory");
		}
		
		
	}
	/*
	 * Use infos of the syscall, such as syscall's name, string parameters, to classify which type of object does this syscall process.
	 */
	public static String object_classifier(SyscallRecord syscall){
		return syscall_to_type.get(syscall.name);		
	}
}
