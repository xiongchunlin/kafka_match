package com.ibm.marple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.apache.hadoop.mapred.ID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import scala.annotation.meta.param;

import com.google.gson.JsonObject;








public class signatureMatch {
	
	
	static public HashSet<String> keysyscallSet = new HashSet<String>();
	static public HashMap<String,HashSet<String>> signatures = new HashMap<String,HashSet<String>>();  //
	
	static public ArrayList<SyscallRecord> records = new ArrayList<SyscallRecord>();
	
	
//	static public HashSet<SyscallRecord> maliciousEvent = new HashSet<SyscallRecord>();
	
	static public HashMap<String, maliciousPoint> maliciousEvent = new HashMap();

	static public Stack<SyscallRecord> gramStack = new Stack<SyscallRecord>();
	
	static public HashMap<String, Integer> UUID2PID = new HashMap<String, Integer>();
	static public HashMap<String, String> uUID2PName = new HashMap<String, String>();
	
	public static void generateNgram(int pos,int deep){
			if(pos<0)
				return;
		// TODO :  add more limits
			
//			ngram.clear();
			

			gramStack.clear();
			SyscallRecord record = records.get(pos);	
			gramStack.push(record);
//			ngram.add(record.name);
			if(signatures.containsKey(record.name))
			{
				for(int i = 0;i<record.id.size();i++)
				if(!maliciousEvent.keySet().contains(record.id.get(i)))
				{
//					outputMaliciousPoint(record,record.name);
					maliciousEvent.put(record.id.get(i),new maliciousPoint(record.id.get(i),record.TimestampMicros.get(i),record.name,record.name));
				}
				else {
					maliciousEvent.get(record.id.get(i)).add(record.name);
				}
//				System.out.println("signature : " + record.name);
//				System.out.println("proccess UUID :" + record.pid);
//				System.out.println("records : " +  record.name + record.id);
//				System.out.println("phf : "+ signatures.get(record.name));
			}
			
			findNgram(pos, deep-1, record.name);
			gramStack.pop();
	}
	
	
	public static void findNgram(int pos,int deep,String proNgram){
		SyscallRecord record = records.get(pos);
		for(int i=Math.max(0, pos-10);i<Math.min(records.size(),pos+10);i++)
			{
				SyscallRecord tmpRecord = records.get(i);
				
				
//				System.out.println(record.pid + " " + tmpRecord.pid);
//				System.out.println(i + " " + pos);
//				System.out.println(tmpRecord.name);
				
				if(i==pos || !keysyscallSet.contains(tmpRecord.name) || !(tmpRecord.pid.equals(record.pid)) || tmpRecord.name.equals(record.name) || gramStack.contains(tmpRecord))
					continue;
//				System.out.println("1");
				gramStack.push(tmpRecord);
				List<Edge> edges = Graph.build_argument_sharing(record, tmpRecord);
//				for(String parm : record.parameters)
//					if(tmpRecord.parameters.contains(parm))
				if (!edges.isEmpty())
					{
						String Ngram = proNgram + "--" + tmpRecord.name;
//						if (deep == 1)
//							System.out.println(Ngram);
//						ngram.add(proNgram);
//						System.out.println(Ngram);
						if(signatures.containsKey(Ngram))
						{
							System.out.println("signature : " + Ngram);
//							System.out.println("proccess ID :" + record.pid);
							for(SyscallRecord a : gramStack)
							{
									for(int i1 = 0;i1<a.id.size();i1++)
									if(!maliciousEvent.keySet().contains(a.id.get(i1)))
									{
										maliciousEvent.put(a.id.get(i1),new maliciousPoint(a.id.get(i1),a.TimestampMicros.get(i1),a.name,Ngram));
										maliciousEvent.get(a.id.get(i1)).addEvidence("PID", a.pid.toString());
										maliciousEvent.get(a.id.get(i1)).addEvidence("processName", a.processName);
										maliciousEvent.get(a.id.get(i1)).addEvidence("Type", Utils.object_classifier(a));
									}
									else {
										maliciousEvent.get(a.id.get(i1)).add(Ngram);
									}
//									System.out.println("signature : " + record.name);
//									System.out.println("proccess UUID :" + record.pid);
//									System.out.println("records : " +  record.name + record.id);
//									System.out.println("phf : "+ signatures.get(record.name));
							}
							
//							System.out.println("phf : "+ signatures.get(Ngram));
						}
						if(!(deep==1))
							findNgram(i, deep-1, Ngram);
						
					}
				gramStack.pop();
			}
	}

	
	
	public static void readSignatures() throws IOException {


		
		//read key system calls.   These system calls are used to determine when to generate ngrams 
		File file1 = new File("./keysyscall/key");
		InputStream is1 = new FileInputStream(file1);
		BufferedReader reader1 = new BufferedReader(new InputStreamReader(is1));
		String keysyscall = reader1.readLine();
		while(keysyscall != null)
		{
//			System.out.println(keysyscall);
			keysyscallSet.add(keysyscall);
			keysyscall = reader1.readLine();		
		}
		
		reader1.close();
		is1.close();
		
		File file2 = new File("./signatures");
		for(File f1 : file2.listFiles())
		{
			InputStream is = new FileInputStream(f1);
			String phf = f1.getName();
			String signatureString; 
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			signatureString = reader.readLine(); 
			while (signatureString != null) { 
				if(signatures.keySet().contains(signatureString))
				{
					HashSet<String> tmpSet = signatures.get(signatureString);
					tmpSet.add(phf);
					signatures.put(signatureString, tmpSet);
				}
				else
				{
					HashSet<String> tmpSet = new HashSet<String>();
					tmpSet.add(phf);
					signatures.put(signatureString,tmpSet);
				}
				signatureString = reader.readLine(); 
//				System.out.println(signatureString);
			}
			reader.close();
			is.close();
		}
		
		
	
		
//		String defaultCDMFile = "data/faros(2).avro";
//		Deserializer ds = new Deserializer();
//		ds.processCDMData(defaultCDMFile);
		
		
	}
	

}
