/**
 * 
 */
package com.ibm.marple;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericContainer;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;

import com.bbn.tc.schema.avro.Event;
import com.bbn.tc.schema.avro.SimpleEdge;
import com.bbn.tc.schema.avro.Subject;
import com.bbn.tc.schema.avro.TCCDMDatum;
import com.bbn.tc.schema.avro.Value;
import com.bbn.tc.schema.serialization.AvroConfig;


public class Deserializer {
	
	private final static AtomicBoolean closed = new AtomicBoolean(false);
	
	private static Properties g_props = new Properties();
	
	private static String groupId;
	
	private static String topic=null;
	
	protected static Producer producer;
	 private static Properties createConsumerProps( String a_groupId ) {
	        Properties props = new Properties();
	        //props.put("zookeeper.connect", a_zookeeper);
	        String bs = g_props.getProperty("bootstrap.servers","localhost:9092");
	        //Log.info("boostrap.servers="+bs);
	        props.put("bootstrap.servers", bs);
	        props.put("group.id", a_groupId);
	        //props.put("zookeeper.session.timeout.ms", "400");
	        //props.put("zookeeper.sync.time.ms", "200");
	        props.put("session.timeout.ms", "30000");
	        props.put("auto.commit.interval.ms", "1000");
	        
	        

	        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
	        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, com.bbn.tc.schema.serialization.kafka.KafkaAvroGenericDeserializer.class);
	        props.put(AvroConfig.SCHEMA_READER_FILE, "schema/TCCDMDatum.avsc");
	        props.put(AvroConfig.SCHEMA_WRITER_FILE, "schema/TCCDMDatum.avsc");
	        props.put(AvroConfig.SCHEMA_SERDE_IS_SPECIFIC,true);
	        // --from-beginning
		    //props.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"smallest");
		    props.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
		    
	 
		    producer = new Producer("localhost:9092", a_groupId, topic);
		    
	        return props;
	    }


	// Static constructor
	{
		System.out.println("---------------------------------------");
		
	}
	
	public static void runKafkaConsumer()
	{
		int count=0;
	    KafkaConsumer consumer = new KafkaConsumer(createConsumerProps(groupId));

		try {
			Schema schema = new Schema.Parser().parse(new File("schema/TCCDMDatum.avsc"));
			DatumReader<TCCDMDatum> datumReader = new SpecificDatumReader<TCCDMDatum>(schema);
            TopicPartition partition0 = new TopicPartition(topic, 0);
            consumer.assign(Arrays.asList(partition0));
            //consumer.subscribe(Arrays.asList(topic));
            //Log.info("Seeking to the beginning of the topic.");
            Set<TopicPartition> partitions = consumer.assignment();
            for (TopicPartition pInfo : partitions)
            {
            	//Log.info("seeking to beginning of partition "+pInfo.partition()+".");
            	consumer.seekToBeginning(pInfo);            	
            }
            boolean exiton0=Boolean.valueOf(g_props.getProperty("exiton0"));
            while (!closed.get()) {
            	//Log.info("Polling...");
                ConsumerRecords<String, GenericContainer> records = consumer.poll(10000);
                //Log.info("received: "+records.count()+" messages...");
                // Handle new records
                Iterator recIter=records.iterator();
                while (recIter.hasNext())
                {
                	ConsumerRecord<String, GenericContainer> record = (ConsumerRecord<String, GenericContainer>)recIter.next();
                	GenericContainer ctr = record.value();
                	TCCDMDatum datum = (TCCDMDatum) record.value();
                	
                	SyscallRecord tmpRecord = parse(datum);
                
                	if(tmpRecord!=null)     //get puuid
   		         {
   		        	 if(recIter.hasNext())
   		        	 {
   		        		record = (ConsumerRecord<String, GenericContainer>)recIter.next();
   	                	ctr = record.value();
   	                	datum = (TCCDMDatum) record.value();
   		        		parse(datum,tmpRecord);
   		        	 }
   		        	 else
   		        	 {
   		        		 tmpRecord = null;
   		        	 }
   		        	 
   		         }
   		         
   			     if(tmpRecord!=null)
   		         {
   		        	 
   		        	 
   		        	 if((count>0) && tmpRecord.name.equals(signatureMatch.records.get(signatureMatch.records.size()-1).name))
   		        	 {
   		        		 signatureMatch.records.get(signatureMatch.records.size()-1).add(tmpRecord);
   		        	 }
   		        	 else{
   		        		 
   		        	 count+=1;
   		        	 signatureMatch.records.add(tmpRecord);
   		        	 if(!signatureMatch.keysyscallSet.contains(tmpRecord.name))
   		        	 {
   		        		 signatureMatch.generateNgram(count-20,3);
//   		        		 signatureMatch.match();
   		        	 }
   		        	 if(count-45>=0)
   		        	 {
   		        		 for(String s : signatureMatch.records.get(count-45).id)
   		        		 {
   		        			 if(signatureMatch.maliciousEvent.keySet().contains(s))
   		        			 {
   		        				 producer.run(signatureMatch.maliciousEvent.get(s).outputRecordAsJson());
   		        			 }
   		        		 }
   		        	 }
   		        	 }  
                }
                if (exiton0)
                {
                		closed.set(true);
                }
            }
    		for(int i = Math.max(0, count-44);i<count;i++)
    		{
    					 for(String s : signatureMatch.records.get(i).id)
    					 {
    						 if(signatureMatch.maliciousEvent.keySet().contains(s))
    							 producer.run(signatureMatch.maliciousEvent.get(s).outputRecordAsJson());
    					 }
    		}
                //
            
            }
		}catch (WakeupException e) {
			// Ignore exception if closing
			if (!closed.get())
				throw e;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			consumer.close();
//			if (neo4j != null) {
//				neo4j.shutdown();
//			}
		}
	}
	
	
	public static SyscallRecord parse(TCCDMDatum record,SyscallRecord tmpRecord) {
		Object datum = record.getDatum();

		if (datum.getClass().getName().endsWith("SimpleEdge"))
		{
			SimpleEdge item = (com.bbn.tc.schema.avro.SimpleEdge)datum;
			CUUID cuuid = new CUUID(item.getToUuid());
			
			tmpRecord.pid = signatureMatch.UUID2PID.get(cuuid.getHexUUID());
			tmpRecord.processName = signatureMatch.uUID2PName.get(cuuid.getHexUUID());
//			System.out.println(tmpRecord.pid);
			return tmpRecord;
		}
		else {
			return null;
		}
	}
	
	public void processCDMData(String cdmFile) {
		int count=0;
	      try {
	    	  
	  		Schema schema = new Schema.Parser().parse(new File("schema/TCCDMDatum.avsc"));
			DatumReader<TCCDMDatum> datumReader = new SpecificDatumReader<TCCDMDatum>(schema);
		    DataFileReader<TCCDMDatum> dataFileReader = null;
			dataFileReader = new DataFileReader(new File(cdmFile), datumReader);
			TCCDMDatum transaction = null;
		     while (dataFileReader.hasNext()) {
		    	 transaction = dataFileReader.next(transaction);
//		         System.out.println(transaction);
		         SyscallRecord tmpRecord = parse(transaction);  
		         
		         if(tmpRecord!=null)     //get puuid
		         {
		        	 if(dataFileReader.hasNext())
		        	 {
		        		 transaction = dataFileReader.next(transaction);
		        		 parse(transaction,tmpRecord);
		        	 }
		        	 else
		        	 {
		        		 tmpRecord = null;
		        	 }
		        	 
		         }
		         
			         if(tmpRecord!=null)
		         {
		        	 
		        	 
		        	 if((count>0) && tmpRecord.name.equals(signatureMatch.records.get(signatureMatch.records.size()-1).name))
		        	 {
		        		 signatureMatch.records.get(signatureMatch.records.size()-1).add(tmpRecord);
		        	 }
		        	 else{
		        		 
		        	 count+=1;
		        	 signatureMatch.records.add(tmpRecord);
		        	 if(!signatureMatch.keysyscallSet.contains(tmpRecord.name))
		        	 {
		        		 signatureMatch.generateNgram(count-20,3);
//		        		 signatureMatch.match();
		        	 }
		        	 if(count-45>=0)
		        	 {
		        		 for(String s : signatureMatch.records.get(count-45).id)
		        		 {
		        			 if(signatureMatch.maliciousEvent.keySet().contains(s))
		        			 {
		        				 signatureMatch.maliciousEvent.get(s).outputRecordAsJson();
		        				 
		        			 }
		        		 }
		        	 }
		        	 }
		         }
//		         System.out.println(count);
		     }
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
//			System.out.println(count);
			for(int i = Math.max(0, count-19);i<count;i++)
			{
				signatureMatch.generateNgram(i,3);
			}
//			System.out.println(signatureMatch.maliciousEvent.size());
 
//			System.out.println(count);
			for(int i = Math.max(0, count-44);i<count;i++)
			{
					 for(String s : signatureMatch.records.get(i).id)
					 {
						 if(signatureMatch.maliciousEvent.keySet().contains(s))
							 signatureMatch.maliciousEvent.get(s).outputRecordAsJson();
					 }
			}
		}
	}
	
	
	public static String unicode2HexString(String unicode) {
		
	    StringBuffer string = new StringBuffer();
	 
	    
	    String[] hex = unicode.split("\\\\u");
	 
	    
	    for (int i = 1; i < hex.length; i++) {
	 
	    	StringBuffer hexStringBuffer = new StringBuffer();
	    	for(int j = 0;j<hex[i].length();j++)
	    	{
	    		int count = 0;
	    		char a = hex[i].charAt(j);
	    		if(count<4 && ((a >= '0' && a <= '9') || (a >= 'A' && a <= 'F')))
	    		{
	    			hexStringBuffer.append(a);
	    			count++;
	    		}
	    	}
	    	
	        int data = Integer.parseInt(hexStringBuffer.toString(), 16);
	 
	        string.append((char) data);
	    }
//	    System.out.println(unicode);
//	    System.out.println(string.toString());
//	    System.exit(0);
	    return string.toString();
	}
	
	public static String unicode2String(String unicode)
	{
		StringBuffer stringBuffer = new StringBuffer();
		for(int i=0;i<unicode.length();i++)
		{
			char a = unicode.charAt(i);
			if((a >= '0' && a <= '9') || (a >= 'A' && a <= 'F'))
			{
				stringBuffer.append(a);
			}
		}
		return stringBuffer.toString();
	}
	
	public static ArrayList<String> getParmString(Event item)
	{ 
		String[] tmpList = item.get("parameters").toString().split("name\": ");
		ArrayList<String> parms = new ArrayList<>();
		for(int i=1;i<tmpList.length;i++)
		{
			parms.add(tmpList[i].split(",")[0]);
		}
//		System.out.println(parms);
		return parms;
	}
	
	public static SyscallRecord parse(TCCDMDatum record)
	{ 
		
		  String name;
		  String id;
		  HashSet<String> parameters = new HashSet<String>();
		  Long TimestampMicros;
		  Long sequence;

		  Object datum = record.getDatum();
//		  System.out.println(datum.getClass().getName());
//		  System.out.println("dataum is an instanceof: "+datum.getClass().getName());
		  
		  if (datum.getClass().getName().endsWith("Event")) {
			  Event item = (com.bbn.tc.schema.avro.Event)datum;
			  CUUID cuuid = new CUUID(item.getUuid());

			  id = cuuid.getHexUUID();
			  name = item.getName().toString();
			  sequence = item.getSequence();
			  TimestampMicros = item.getTimestampMicros();
//			  System.out.println(getParmString(item).toString());
//			  System.exit(0);
			  ArrayList<String> parmStrings = getParmString(item);
			  
			  List<Value> parms = item.getParameters();
			  if(parms!=null)
			  {
				  for (int j=0;j<parms.size();j++)
				  {
					  if(parmStrings.get(j).equals("null"))
					  {
						  parameters.add(unicode2String(parms.get(j).toString().split("valueBytes")[1].split("\"")[4]));
					  }
					  else {
						parameters.add(parmStrings.get(j));
//						System.out.println(parmStrings.get(j));
					}
				  }
				  
			  }
			  
			  return new SyscallRecord(id, name, parameters,TimestampMicros,sequence);
		  }
		  else {
			  if(datum.getClass().getName().endsWith("Subject"))
			  {
				  Subject item = (Subject)datum;
//				  System.out.println();
				  if(item.getType().toString().equals("SUBJECT_PROCESS"))
				  {
					  signatureMatch.UUID2PID.put(new CUUID(item.getUuid()).getHexUUID(),item.getPid());
					  signatureMatch.uUID2PName.put(new CUUID(item.getUuid()).getHexUUID(), item.getCmdLine().toString());
//					  System.out.println(item.getCmdLine());
//					  System.out.println(item.getPid());
//					  System.exit(0);
				  }
			  }
			  return null;
			//ignore the currect record
		}
		  
	}
	
	public static void setGroupId(String groupid)
	{
		groupId = groupid;
	}
	
	public static void main(String args[]) throws IOException
    {
    	signatureMatch.readSignatures();
		String defaultCDMFile = "data/livetest_1.bin";
//		defaultCDMFile = g_props.getProperty("cdmfile",defaultCDMFile);
		//ds.processLabeledGraph_ta3();
		Random r = new Random();
		setGroupId("MARPLE"+String.valueOf(r.nextLong()));
//		runKafkaConsumer();	

    }
}
