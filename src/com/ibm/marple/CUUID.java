/**
 * 
 */
package com.ibm.marple;

import com.bbn.tc.schema.avro.UUID;

/**
 * @author habeck
 * (C) IBM Corporation 2016 All Rights Reserved
 * WRAPPER class to consistently UUID values from CDM UUIDs.
 * 
 */
public class CUUID implements Comparable {

	private UUID uuid;
	
	private String hexUUID=null;
	
	public String getHexUUID()
	{
		if (hexUUID==null)
		{
			byte a[] = uuid.bytes();
			StringBuffer sb = new StringBuffer();
			for (int i=0;i<a.length;i++)
			{
				//sb.append(Integer.toHexString(a[i]));
				sb.append(String.format("%02x", a[i]));
			}
			hexUUID= sb.toString();
		}
//		System.out.println(hexUUID);
		return hexUUID;
	}
	
	public UUID getUUID()
	{
		return uuid;
	}
	
	
	public CUUID(UUID uuid)
	{
		this.uuid = uuid;
		String wdc = getHexUUID();
	}

	
	
	public int compareTo(CUUID that) {

		return hexUUID.compareTo(that.getHexUUID());
		//return super.compareTo(that);
	}
	
	
	public byte[] bytes()
	{
		return this.uuid.bytes();
	}


	public int compareTo(Object o) {
		if (o instanceof CUUID )
		{
			return compareTo((CUUID)o);
		} else {
	     	return 1;
		}
	}
	
}
